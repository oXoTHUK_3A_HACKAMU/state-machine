﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Pool : MonoBehaviour
{
    public static Pool Instance = null;
    [System.Serializable]
    public class PoolObject
    {
        public GameObject prefab;
        public int amount;
    }
    public List<PoolObject> poolObjects;
    public List<GameObject> objectsOnScene;
    [SerializeField][Header("Координаты родительского объекта")] Transform spawner;

    private void Start()
    {
        StartCoroutine(spawnEnemy());
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        foreach (PoolObject item in poolObjects)
        {
            for (int i = 0; i < item.amount; i++)
            {
                if(item.prefab.tag == "Enemy")
                {
                    GameObject objEnemy = Instantiate(item.prefab, new Vector3(UnityEngine.Random.Range(-10, 10), 20, 0), Quaternion.identity, spawner);
                    objectsOnScene.Add(objEnemy);
                    objEnemy.SetActive(false);
                }

                if (item.prefab.tag == "Bullet")
                {
                    GameObject objBullet = Instantiate(item.prefab, spawner);
                    objectsOnScene.Add(objBullet);
                    objBullet.SetActive(false);
                }
                //GameObject obj = Instantiate(item.prefab, spawner);
                //objectsOnScene.Add(obj);
                //obj.SetActive(false);
            }
        }
    }
    public GameObject Get(string tag)
    {
         foreach (GameObject item in objectsOnScene)
         {
             if(item.CompareTag(tag) && !item.activeInHierarchy)
             {
                 item.SetActive(true);
                 item.GetComponent<Rigidbody>().velocity = new Vector3(0, 15, 0);
                 return item;
             }
             if (item.CompareTag(tag) && !item.activeInHierarchy)
             {
                item.SetActive(true);
                return item;
             }
         }
         return null;
    }

    IEnumerator spawnEnemy()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            Get("Enemy");
        }
    }
}