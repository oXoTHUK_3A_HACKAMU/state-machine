﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPoints : MonoBehaviour
{
    public GameObject[] myWaypoints;
    int myWaypointCount = 0;
    public int moveSpeed = 5;
    void EnemyMovement()
    {

        transform.position = Vector3.MoveTowards(transform.position, myWaypoints[myWaypointCount].transform.position, moveSpeed * Time.deltaTime);
        if (Vector3.Distance(myWaypoints[myWaypointCount].transform.position, transform.position) <= 2f)
        {
            myWaypointCount++;
        }

        if (myWaypointCount >= myWaypoints.Length)
        {
            myWaypointCount = 0;
        }
    }

    void OnDrawGizmos() //Встроенный метод,необходим для работы с Gizmos объектами и их отрисовки
    {
        Gizmos.color = Color.red; //Назначаем цвет нашему объекту

        //Рисуем Gizmos Куб.Он принимает два параметра. 1.Позиция Объекта 2.Размер Объекта
        for (int i = 0; i < myWaypoints.Length; i++)
        {
            Gizmos.DrawCube(myWaypoints[i].transform.position, transform.localScale);
        }
        
    }
    void Update()
    {
        EnemyMovement();
    }
}