﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    private UnityEngine.AI.NavMeshAgent NMA;
    Animator animator;

    private void Start()
    {
        NMA = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            animator.SetBool("Walk", true);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit Pos;
            if (Physics.Raycast(ray, out Pos))
            {
                NMA.SetDestination(Pos.point);
            }
        }
    }
}
